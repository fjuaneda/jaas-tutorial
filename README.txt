A personnal rewriting of the old Oracle tutorial on JAAS Authentication and Authorization.
Better than the orginal in many ways although not perfect. Do not have time to provide a
cleaner version though.

Sources:
    http://docs.oracle.com/javase/8/docs/technotes/guides/security/jaas/JAASRefGuide.html
    http://docs.oracle.com/javase/8/docs/technotes/guides/security/PolicyFiles.html
    http://docs.oracle.com/javase/8/docs/technotes/guides/security/jaas/tutorials/GeneralAcnOnly.html
    http://docs.oracle.com/javase/8/docs/technotes/guides/security/jaas/tutorials/GeneralAcnAndAzn.html

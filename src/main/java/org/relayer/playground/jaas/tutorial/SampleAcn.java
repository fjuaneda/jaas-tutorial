package org.relayer.playground.jaas.tutorial;

import java.io.*;
import javax.security.auth.callback.*;
import javax.security.auth.login.*;

/**
 * This Sample application attempts to authenticate a user and reports whether or not the
 * authentication was successful.
 */
public class SampleAcn {
    public static void main(final String... args) {
        if(System.console() == null) {
            System.err.println("No console supported.");
            System.exit(-1);
        }

        LoginContext lc = null;

        try {
            // Retrieve login context for Sample configuration supplying a custom cb
            lc = new LoginContext("Sample", new MyCallbackHandler());
        } catch(final LoginException | SecurityException se) {
            System.err.println("Cannot create LoginContext. ".concat(se.getMessage()));
            System.exit(-1);
        }

        boolean authenticated = false;

        // 3 login attemps allowed
        for(int i = 0; !authenticated && (i < 3); i++) {
            try {
                // Try login
                lc.login();

                authenticated = true;
            } catch(final LoginException le) {
                System.err.println("Authentication failed:".concat(le.getMessage()));
            }
        }

        if(!authenticated) {
            System.out.println("Sorry, authentication failed.");
            System.exit(-1);
        }

        System.out.println("Congratulations, authentication succeeded!");
    }
}

/**
 * The application implements the CallbackHandler.
 *
 * This application is text-based.  Therefore it displays information to the user using the OutputStreams
 * System.out and System.err, and gathers input from the user using the InputStream System.in.
 */
class MyCallbackHandler implements CallbackHandler {
    /**
     * Invoke an array of Callbacks.
     *
     * @param callbacks an array of <code>Callback</code> objects which contain the information requested by an
     *                  underlying security service to be retrieved or displayed.
     *
     * @exception java.io.IOException if an input or output error occurs.
     *
     * @exception UnsupportedCallbackException if the implementation of this method does not support one or more
     *                  of the Callbacks specified in the <code>callbacks</code> parameter.
     */
    @Override
    public void handle(final Callback[] callbacks)
        throws
            IOException,
            UnsupportedCallbackException
    {
        for(final Callback cb : callbacks) {
            if (cb instanceof TextOutputCallback) {
                // display the message according to the specified type
                final TextOutputCallback toc = (TextOutputCallback) cb;

                switch (toc.getMessageType()) {
                    case TextOutputCallback.INFORMATION:
                        System.out.println(toc.getMessage());
                        break;
                    case TextOutputCallback.ERROR:
                        System.out.println("ERROR: ".concat(toc.getMessage()));
                        break;
                    case TextOutputCallback.WARNING:
                        System.out.println("WARNING: ".concat(toc.getMessage()));
                        break;
                    default:
                        throw new IOException("Unsupported message type: ".concat(String.valueOf(toc.getMessageType())));
                }
            } else if(cb instanceof NameCallback) {
                // Prompt for username
                final NameCallback nc = (NameCallback) cb;
                System.err.print(nc.getPrompt());
                System.err.flush();

                nc.setName(System.console().readLine());
            } else if (cb instanceof PasswordCallback) {
                // Prompt for password
                final PasswordCallback pc = (PasswordCallback) cb;
                System.err.print(pc.getPrompt());
                System.err.flush();

                pc.setPassword(readPassword());
            } else {
                throw new UnsupportedCallbackException(cb, "Unrecognized Callback");
            }
        }
    }

    // Reads user password from console
    private char[] readPassword() throws IOException {
        return System.console().readPassword("[%s]", "Please enter password: ");
    }
}

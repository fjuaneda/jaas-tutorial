package org.relayer.playground.jaas.tutorial.module;

import java.io.IOException;
import java.util.*;
import javax.security.auth.*;
import javax.security.auth.callback.*;
import javax.security.auth.login.*;
import javax.security.auth.spi.*;
import org.relayer.playground.jaas.tutorial.principal.SamplePrincipal;

/**
 * This sample LoginModule authenticates users with a password.
 *
 * <p> This LoginModule only recognizes one user: testUser
 * <p> testUser's password is: testPassword
 *
 * <p> If testUser successfully authenticates itself,
 * a <code>SamplePrincipal</code> with the testUser's user name is added to the Subject.
 *
 * <p> This LoginModule recognizes the debug option. If set to true in the login Configuration,
 * debug messages will be output to the output stream, System.out.
 */
public class SampleLoginModule implements LoginModule {
    // initial state
    private Subject subject;
    private CallbackHandler callbackHandler;
    private Map sharedState;
    private Map options;

    // configurable option
    private boolean debug = false;

    // the authentication status
    private boolean succeeded = false;
    private boolean commitSucceeded = false;

    // username and password
    private String username;
    private char[] password;

    // testUser's SamplePrincipal
    private SamplePrincipal userPrincipal;

    /**
     * Initialize this <code>LoginModule</code>.
     *
     * @param subject the <code>Subject</code> to be authenticated. <p>
     * @param callbackHandler a <code>CallbackHandler</code> for communicating with the end user (prompting for user names and
     *                  passwords, for example). <p>
     * @param sharedState shared <code>LoginModule</code> state. <p>
     * @param options options specified in the login <code>Configuration</code> for this particular <code>LoginModule</code>.
     */
    @Override
    public void initialize(
        final Subject subject,
        final CallbackHandler callbackHandler,
        final Map<java.lang.String, ?> sharedState,
        final Map<java.lang.String, ?> options
    ) {
        this.subject = subject;
        this.callbackHandler = callbackHandler;
        this.sharedState = sharedState;
        this.options = options;

        // initialize any configured options
        debug = "true".equalsIgnoreCase((String) options.get("debug"));
    }

    /**
     * Authenticate the user by prompting for a user name and password.
     *
     * @return true in all cases since this <code>LoginModule</code> should not be ignored.
     * @exception FailedLoginException if the authentication fails. <p>
     * @exception LoginException if this <code>LoginModule</code> is unable to perform the authentication.
     */
    @Override
    public boolean login() throws LoginException {
        // prompt for a user name and password
        if(callbackHandler == null)
            throw new LoginException("Error: no CallbackHandler available to garner authentication information from the user");

        final Callback[] callbacks = new Callback[2];
        final int USER_NAME_CB_IDX = 0;
        final int PWD_CB_IDX = 1;
        callbacks[USER_NAME_CB_IDX] = new NameCallback("User name: ");
        callbacks[PWD_CB_IDX] = new PasswordCallback("Password: ", false);

        try {
            callbackHandler.handle(callbacks);
            username = ((NameCallback)callbacks[USER_NAME_CB_IDX]).getName();

            char[] tmpPassword = ((PasswordCallback) callbacks[PWD_CB_IDX]).getPassword();
            if (tmpPassword == null) {
                // Process a NULL password as an empty password
                tmpPassword = new char[0];
            }

            password = new char[tmpPassword.length];
            System.arraycopy(tmpPassword, 0, password, 0, tmpPassword.length);

            ((PasswordCallback)callbacks[1]).clearPassword();
        } catch(final IOException ioe) {
            throw new LoginException(ioe.toString());
        } catch(final UnsupportedCallbackException uce) {
            throw new LoginException(
                "Error: " + uce.getCallback().toString() +
                " not available to garner authentication information from the user");
        }

        // Print debugging information
        if(debug) {
            System.out.println("\t\t[SampleLoginModule] user entered user name: ".concat(username));
            System.out.print("\t\t[SampleLoginModule] user entered password: ");

            for(int i = 0; i < password.length; i++) System.out.print(password[i]);
            System.out.println();
        }

        // Verify username/password
        boolean usernameCorrect = false;
        boolean passwordCorrect = false;

        if(username.equals("testUser"))
            usernameCorrect = true;

        if(usernameCorrect &&
            password.length == 12 &&
            password[0] == 't' &&
            password[1] == 'e' &&
            password[2] == 's' &&
            password[3] == 't' &&
            password[4] == 'P' &&
            password[5] == 'a' &&
            password[6] == 's' &&
            password[7] == 's' &&
            password[8] == 'w' &&
            password[9] == 'o' &&
            password[10] == 'r' &&
            password[11] == 'd'
        ) {
            // Authentication succeeded!!!
            passwordCorrect = true;

            if (debug)
                System.out.println("\t\t[SampleLoginModule] authentication succeeded");

            succeeded = true;
            return true;
        } else {
            // Authentication failed -- clean out state
            if (debug)
                System.out.println("\t\t[SampleLoginModule] authentication failed");
            succeeded = false;
            username = null;

            Arrays.fill(password, ' ');
            password = null;

            if (!usernameCorrect) {
                throw new FailedLoginException("User name incorrect");
            } else {
                throw new FailedLoginException("Password incorrect");
            }
        }
    }

    /**
     * <p> This method is called if the LoginContext's overall authentication succeeded
     * (the relevant REQUIRED, REQUISITE, SUFFICIENT and OPTIONAL LoginModules succeeded).
     *
     * <p> If this LoginModule's own authentication attempt
     * succeeded (checked by retrieving the private state saved by the
     * <code>login</code> method), then this method associates a
     * <code>SamplePrincipal</code>
     * with the <code>Subject</code> located in the
     * <code>LoginModule</code>.  If this LoginModule's own
     * authentication attempted failed, then this method removes
     * any state that was originally saved.
     *
     * <p>
     *
     * @exception LoginException if the commit fails.
     *
     * @return true if this LoginModule's own login and commit
     *          attempts succeeded, or false otherwise.
     */
    @Override
    public boolean commit() throws LoginException {
        if(succeeded == false) {
            return false;
        } else {
            // Add a Principal(authenticated identity) to the Subject assumed to be the SamplePrincipal
            userPrincipal = new SamplePrincipal(username);

            if(!subject.getPrincipals().contains(userPrincipal))
                subject.getPrincipals().add(userPrincipal);

            if (debug) {
                System.out.println("\t\t[SampleLoginModule] added SamplePrincipal to Subject");
            }

            // In any case, clean out state
            username = null;
            Arrays.fill(password, ' ');
            password = null;

            commitSucceeded = true;
            return true;
        }
    }

    /**
     * <p> This method is called if the LoginContext's overall authentication failed.
     * (the relevant REQUIRED, REQUISITE, SUFFICIENT and OPTIONAL LoginModules
     * did not succeed).
     *
     * <p> If this LoginModule's own authentication attempt
     * succeeded (checked by retrieving the private state saved by the
     * <code>login</code> and <code>commit</code> methods),
     * then this method cleans up any state that was originally saved.
     *
     * <p>
     *
     * @exception LoginException if the abort fails.
     *
     * @return false if this LoginModule's own login and/or commit attempts
     *          failed, and true otherwise.
     */
    @Override
    public boolean abort() throws LoginException {
        if(succeeded == false) {
            return false;
        } else if(succeeded == true && commitSucceeded == false) {
            // login succeeded but overall authentication failed
            succeeded = false;
            username = null;

            if(password != null) {
                Arrays.fill(password, ' ');
                password = null;
            }
            userPrincipal = null;
        } else {
            logout();
        }
        return true;
    }

    /**
     * Logout the user.
     *
     * <p> This method removes the <code>SamplePrincipal</code>
     * that was added by the <code>commit</code> method.
     *
     * <p>
     *
     * @exception LoginException if the logout fails.
     *
     * @return true in all cases since this <code>LoginModule</code>
     *          should not be ignored.
     */
    @Override
    public boolean logout() throws LoginException {
        subject.getPrincipals().remove(userPrincipal);
        succeeded = false;
        succeeded = commitSucceeded;

        username = null;
        if (password != null) {
            Arrays.fill(password, ' ');
            password = null;
        }
        userPrincipal = null;
        return true;
    }
}
